package Wolf_Fox;

import java.io.*;

public class Files03 {

  public static void main(String[] args) throws Exception {
    BufferedReader r = new BufferedReader(new FileReader("C:\\Users\\Love your Dad\\data/wolf-fox.txt"));

    r.lines().filter(l -> l.contains("his"))
        .forEach(l -> System.out.println(l));

    r.close();
  }

}
