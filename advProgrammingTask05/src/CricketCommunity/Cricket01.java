package CricketCommunity;

import java.util.Arrays;
import java.util.List;

public class Cricket01 {
  public static void main(String[] args) {
    List<CricketClub> table = Arrays.asList(
            new CricketClub(1, "Sri Lanka", 60, 70, 80, 150, 121, 100, 120, 75, 80,
                    8, 2, 76),
                new CricketClub(2, "England", 55, 16, 0, 20, 125, 41, 21, 72, 43, 9, 2, 75),
                new CricketClub(3, "Australia", 22, 55, 1, 26, 153, 121, 32, 45, 39, 34,
                    2, 68),
                new CricketClub(4, "India", 22, 14, 1, 7, 34, 118, 146, 70, 40, 5, 10, 28),
                new CricketClub(5, "New Zealand", 22, 14, 0, 8, 663, 437, 226, 70, 46, 5, 7,
                    68),
                new CricketClub(6, "Pakistan", 32, 11, 2, 9, 62, 27, 145, 77, 54, 9, 14, 21),
                new CricketClub(7, "South Africa", 32, 21, 0, 11, 97, 42, 15, 62, 24, 6, 14,
                    54),
                new CricketClub(8, "Scotland", 22, 10, 0, 12, 44, 14, -40, 45, 50, 4, 25,
                    49),
                new CricketClub(9, "East Africa", 12, 9, 1, 12, 153, 575, -22, 33, 61, 4, 6,
                    48),
                new CricketClub(10, "Ireland", 22, 7, 1, 14, 42, 178, -136, 46, 57, 4, 12,
                    30),
                new CricketClub(11, "Bangladesh", 22, 5, 1, 16, 45, 55, -70, 17, 61,
                    14, 8, 44),
                new CricketClub(12, "Netherlands", 22, 0, 0, 22, 23, 11, -78, 29, 47, 1,
                    0, 10));

     table.forEach(x -> System.out.println(x));
  }

}
