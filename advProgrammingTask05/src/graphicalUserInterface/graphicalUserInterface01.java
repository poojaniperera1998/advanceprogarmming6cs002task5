package graphicalUserInterface;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
public class graphicalUserInterface01 {
		public static void main(String[] args) {
		    JFrame Jframe = new JFrame();
		    JButton Jbutton = new JButton("Go In");
		    Jframe.setSize(300,300);
		    Jframe.setLayout(new FlowLayout());
		    Jframe.add(Jbutton, BorderLayout.CENTER);
		    
		    Jbutton.addActionListener(new ActionListener() {
		      public void actionPerformed(ActionEvent event) {
		        System.out.println("Button Print: " + 
		          event.getActionCommand());        
		      }
		    });
		        
		    Jframe.setVisible(true);
		  }
}
