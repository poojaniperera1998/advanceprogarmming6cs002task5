package graphicalUserInterface;
import java.awt.*;
import javax.swing.*;
public class graphicalUserInterface02 {
		  public static void main(String[] args) {
			    JFrame Jframe = new JFrame("GUI One");
			    JButton Jbutton = new JButton("Go In");
			    Jframe.setSize(300,300);
			    Jframe.setLayout(new FlowLayout());
			    Jframe.add(Jbutton, BorderLayout.CENTER);
			    Jbutton.addActionListener(
			        event -> System.out.println("Button Print: " + 
			                   event.getActionCommand()));
			    Jframe.setVisible(true);
			  }
}
